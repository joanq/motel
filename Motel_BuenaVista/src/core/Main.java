package core;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import com.violentbits.dbutils.dao.DataAccess;

import generators.Generators;
import readers.ConfigReader;


public class Main {
	/**
	 * Here is where start, u need to execute this. Only call Menu method and menu
	 * do everything.
	 */
	public static void main(String[] args) {
		try {
			System.out.println("Welcome to Motel BuenaVista DB generator.");
			System.out.println("----");
			ConfigReader conf = new ConfigReader();
			conf.readConf();
			DataAccess.init(conf.getURL(), conf.getUser(), conf.getPassword());
			Generators.init(conf.getStartDate(), conf.getEndDate());
			new Core(conf.getStartDate(), conf.getEndDate(), conf.getNRooms());
			Core.log("Dumping database, please wait...");
			if(args.length != 0) {
				dump(args[0]);
			}else {
				dump("hotel");
			}
			Core.log("Database dumped.");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void dump(String f) {
		File file = new File(f + ".sql");
		Dumper dump  = new Dumper();
		try {
			dump.dumpMysql(file);
		} catch (InterruptedException | IOException | SQLException e) {
			e.printStackTrace();
		}
	}
	

}
