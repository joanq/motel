package core;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.violentbits.dbutils.dao.DataAccess;

import factories.BookingFactory;
import factories.RoomFactory;
import factories.StayFactory;
import generators.FacilityGenerator;
import generators.Generators;
import generators.PriceSeasonsGenerator;
import generators.SeasonsGenerator;
import jdbc.CreateTablesJDBC;
import jdbc.FacilitiesJDBC;
import jdbc.PriceSeasonsJDBC;
import jdbc.RoomJDBC;
import jdbc.RoomTypeFacilitiesJDBC;
import jdbc.RoomTypeJDBC;
import jdbc.SeasonsJDBC;
import models.Facilities;
import models.PriceSeason;
import models.Room;
import models.RoomType;
import models.RoomTypeFacilities;
import models.Season;
import readers.Reader;

public class Core {
	private int nRooms;
	private LocalDate startDate;
	private LocalDate endDate;
	private static long startTime=System.currentTimeMillis();
	
	public static void log(String message) {
		System.out.format("%.3f: %s\n", (System.currentTimeMillis()-startTime)/1000.0, message);
	}
	
	/**
	 * Customer got a field for the numbers of customers. create Factorys.
	 * 
	 */
	public Core(LocalDate startDate, LocalDate endDate, int nRooms) throws IOException, SQLException {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.nRooms = nRooms;
		log("Creating database structure...");
		createTables();
		log("Database succesfully created.\n");
		DataAccess.getInstance().executeQuery("SET @@foreign_key_checks = 0;");
		DataAccess.getInstance().getConnection().setAutoCommit(false);
		log("Inserting RoomTypes");
		List<RoomType> roomTypes = roomTypeCore();
		log("Inserting Facilities");
		facilitiesCore();
		log("Inserting RoomTypeFacilities");
		roomTypeFacilities();
		log("Inserting Seasons");
		List<Season> seasons = seasonCore();
		log("Inserting PriceSeasons");
		priceSeasonCore(seasons, roomTypes);
		log("Inserting Rooms");
		roomsCore();
		DataAccess.getInstance().getConnection().commit();
		timeCore();
		log("Database Succesfully Generated!\n");
		DataAccess.getInstance().executeQuery("DROP TABLE PriceSeasonsCache");
		DataAccess.getInstance().executeQuery("DROP TABLE SeasonsCache");
		DataAccess.getInstance().executeQuery("DROP TABLE PendingBookings");
		DataAccess.getInstance().executeQuery("ALTER TABLE RoomTypes DROP COLUMN BasePrice");
	}
	
	public void timeCore() throws SQLException, IOException {
		long timeCheckOut=0, timeBookings=0, timeStays=0, tmpTime;
		BookingFactory bookingFactory = BookingFactory.getInstance();
		StayFactory stayFactory = StayFactory.getInstance();
		int nDays=0;
		for (LocalDate currentDate = LocalDate.from(startDate);
				currentDate.isBefore(endDate); currentDate = currentDate.plusDays(1)) {
			if (nDays%100==0) {
				DataAccess.getInstance().getConnection().commit();
				log("Current date: "+currentDate);
				log(String.format("Time spent: CheckOuts: %.3f, Customers: %.3f, RoomTypes: %.3f, "
						+ "Prices: %.3f, Inserts: %.3f, Stays: %.3f",
						timeCheckOut/1000.0,
						BookingFactory.getInstance().timeCustomers/1000.0,
						BookingFactory.getInstance().timeRoomTypes/1000.0,
						BookingFactory.getInstance().timePrices/1000.0,
						BookingFactory.getInstance().timeInserts/1000.0,
						timeBookings/1000.0, timeStays/1000.0));
			}
			// Check out rooms
			tmpTime=System.currentTimeMillis();
			stayFactory.checkOutStays(currentDate);
			timeCheckOut+=System.currentTimeMillis()-tmpTime;
			// New reservations
			tmpTime=System.currentTimeMillis();
			int nBookings = ThreadLocalRandom.current().nextInt(nRooms/10, nRooms);
			for (int nBooking = 0; nBooking<nBookings; nBooking++) {
				bookingFactory.createBooking(currentDate);
			}
			timeBookings+=System.currentTimeMillis()-tmpTime;
			// Stays
			tmpTime=System.currentTimeMillis();
			stayFactory.createStays(currentDate, endDate);
			timeStays+=System.currentTimeMillis()-tmpTime;
			nDays++;
		}
		DataAccess.getInstance().getConnection().commit();
	}
	
	public List<RoomType> roomTypeCore() throws IOException, SQLException {
		List<RoomType> allRoomTypes = Generators.roomTypeGenerator.getAllRoomTypes();
		RoomTypeJDBC roomTypeJdbc = new RoomTypeJDBC();
		roomTypeJdbc.insertRoomTypes(allRoomTypes);
		return allRoomTypes;
	}

	public List<Facilities> facilitiesCore() throws SQLException {
		List<Facilities> returnList = null;

		FacilityGenerator fg = Generators.facilityGenerator;
		returnList = fg.getFacilities();
		FacilitiesJDBC facilityJdbc = new FacilitiesJDBC();
		facilityJdbc.insertFacilities(returnList);
		return returnList;
	}

	public List<Season> seasonCore() throws SQLException {
		List<Season> seasons = new ArrayList<>();

		SeasonsGenerator sg = Generators.seasonsGenerator;
		SeasonsJDBC seasonsjdbc = new SeasonsJDBC();
		seasons = sg.getSeasons();
		seasonsjdbc.insertSeasons(seasons);
		return seasons;
	}

	public List<PriceSeason> priceSeasonCore(List<Season> seasons, List<RoomType> roomTypes) throws SQLException {
		List<PriceSeason> returnList = null;
		
		PriceSeasonsGenerator psg = Generators.priceSeasonsGenerator;
		PriceSeasonsJDBC psjdbc = new PriceSeasonsJDBC();
		psjdbc.insertPriceSeasons(psg.createPriceSeasons(seasons, roomTypes));
		return returnList;
	}

	public List<RoomTypeFacilities> roomTypeFacilities() throws SQLException, IOException {
		List<RoomTypeFacilities> returnList = Generators.roomTypeFacilitiesGenerator.getList();
		RoomTypeFacilitiesJDBC rtfJDBC = new RoomTypeFacilitiesJDBC();
		rtfJDBC.insertRoomTypesFacilities(returnList);
		return returnList;
	}

	public List<Room> roomsCore() throws SQLException {
		List<Room> returnList = null;
		RoomFactory rf = new RoomFactory();
		RoomJDBC rjdbc = new RoomJDBC();
		rjdbc.insertRooms(rf.createRooms(nRooms));
		return returnList;
	}
	
	public void createTables() throws IOException, SQLException {
		CreateTablesJDBC.createTables(Reader.createTables());
	}
}
