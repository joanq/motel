package core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.violentbits.dbutils.dao.DataAccess;

import readers.ConfigReader;

public class Dumper {

	public void dumpMysql(File f) throws InterruptedException, IOException, SQLException {
		File file = f;
		String osName = System.getProperty("os.name");
		if (isWindows(osName)) {
			throw new UnsupportedOperationException("The dump for Windows has not been implemented yet.");
			//dumpForWindows(file);
		} else {
			dumpForLinux(file);
		}
	}

	// Not working fine, working to fix it.
	public void dumpForWindows(File f) throws IOException, InterruptedException, SQLException {
		// define backup file
		ConfigReader reader = new ConfigReader();
		reader.readConf();
		String dumpCommand = "cmd.exe/ " + getMysqlPath() + "\\" + "mysqldump.exe" + "\\"
				+ "--quick --lock-tables --user=" + reader.getUser() + "--password=" + reader.getPassword()
				+ " --databases" + reader.getDB();
		Runtime rt = Runtime.getRuntime();
		File test = f;
		Process child = rt.exec(dumpCommand);
		try (PrintStream ps = new PrintStream(test)) {
			InputStream in = child.getInputStream();
			int ch;
			while ((ch = in.read()) != -1) {
				ps.write(ch);
			}
			InputStream err = child.getErrorStream();
			while ((ch = err.read()) != -1) {
				System.out.write(ch);
			}
		}
	}

	public void dumpForLinux(File f) throws InterruptedException, IOException {
		ConfigReader reader = new ConfigReader();
		reader.readConf();

		String dumpCommand = "mysqldump --add-drop-database -B " + reader.getDB() + " -h " + reader.getIP() + " -u " + reader.getUser() + " -p"
				+ reader.getPassword();
		Runtime rt = Runtime.getRuntime();
		File test = f;
		try (PrintStream ps = new PrintStream(test)){
			Process child = rt.exec(dumpCommand);
			
			InputStream in = child.getInputStream();
			int ch;
			while ((ch = in.read()) != -1) {
				ps.write(ch);
			}
			InputStream err = child.getErrorStream();
			while ((ch = err.read()) != -1) {
				System.out.write(ch);
			}
		}
	}

	public boolean isWindows(String os) {
		if (os.contains("Windows"))
			return true;
		return false;
	}

	public String getMysqlPath() throws IOException, SQLException {
		String sql = "SELECT @@basedir";
		String basedir = null;
		
		try (ResultSet rs = DataAccess.getInstance().executeQuery(sql);) {
			while (rs.next()) {
				basedir = rs.getString("@@basedir");
			}
		}
		return basedir;
	}

}
