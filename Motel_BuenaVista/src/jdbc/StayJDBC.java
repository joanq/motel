package jdbc;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import com.violentbits.dbutils.dao.DataAccess;

import models.Stay;

public class StayJDBC {
	public void checkOut(LocalDate date) throws SQLException {
		String sql = "UPDATE Rooms SET Empty=true WHERE RoomNumber IN("
				+ "SELECT RoomNumber FROM Stays "
				+ "WHERE CheckOut=?"
				+ ")";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setDate(1, Date.valueOf(date));
		st.executeUpdate();
	}
	
	public void insertStay(Stay stay) throws SQLException {
		String sql = "INSERT INTO Stays(CheckIn, RoomNumber,CheckOut,TotalPrice,PaymentType,PaymentDateTime,BookingId) VALUES (?, ?,?,?,?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
		
		statement.setDate(1, Date.valueOf(stay.getCheckIn()));
		statement.setInt(2, stay.getRoomNumber());
		statement.setDate(3, stay.getCheckOut()!=null?Date.valueOf(stay.getCheckOut()):null);
		statement.setDouble(4, stay.getTotalPrice());
		statement.setString(5, stay.getPaymentType());
		statement.setString(6, stay.getPaymentDateTime()!=null?stay.getPaymentDateTime().toString():null);
		statement.setInt(7, stay.getBookingId());
		statement.executeUpdate();
		
		ResultSet rs = statement.getGeneratedKeys();
		if (rs.next()) {
			stay.setId(rs.getInt(1));
		}
	}
	
	public Integer getStayIdForCustomer(int customerId) throws SQLException {
		Integer stayId = null;
		String sql = "SELECT s.* FROM Stays s "
				+ "JOIN Bookings b ON s.BookingId=b.Id "
				+ "WHERE b.CustomerId = ? "
				+ "ORDER BY RAND() LIMIT 1";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setInt(1, customerId);
		try (ResultSet rs = st.executeQuery()) {
			if (rs.next()) {
				stayId = rs.getInt(1);
			}
		}
		return stayId;
	}
}
