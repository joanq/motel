package jdbc;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;

import models.PriceSeason;

public class PriceSeasonsJDBC {

	public void insertPriceSeasons(List<PriceSeason> priceSeasons) throws SQLException {
		String sql = "INSERT INTO PriceSeasons (SeasonId,RoomTypeId,Price) VALUES (?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
		for (PriceSeason s : priceSeasons) {
			statement.setInt(1, s.getSeason().getId());
			statement.setInt(2, s.getRoomType().getId());
			statement.setDouble(3, s.getPrice());
			statement.addBatch();
		}
		statement.executeBatch();
		insertPriceSeasonsCache(priceSeasons);
	}
	
	private void insertPriceSeasonsCache(List<PriceSeason> priceSeasons) throws SQLException {
		String sql = "INSERT INTO PriceSeasonsCache(SeasonId,RoomTypeId,Price) VALUES (?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql);
		for (PriceSeason s : priceSeasons) {
			statement.setInt(1, s.getSeason().getId());
			statement.setInt(2, s.getRoomType().getId());
			statement.setDouble(3, s.getPrice());
			statement.addBatch();
		}
		statement.executeBatch();
	}

	public double getBookingPrice(int roomTypeId, LocalDate date) throws SQLException {
		double priceseason = 0;
		String sql = "SELECT Price FROM PriceSeasonsCache ps "
				+ "JOIN SeasonsCache s ON ps.SeasonId=s.Id "
				+ "WHERE RoomTypeId = ? AND StartingDay <= ? " 
				+ "ORDER BY StartingDay DESC LIMIT 1";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setInt(1, roomTypeId);
		st.setDate(2, Date.valueOf(date));

		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				priceseason = rs.getDouble("Price");
			}
		}
		return priceseason;
	}
}
