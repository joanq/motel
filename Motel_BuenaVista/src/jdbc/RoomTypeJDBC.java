package jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;

import models.RoomType;

public class RoomTypeJDBC {

	public void insertRoomTypes(List<RoomType> roomTypes) throws SQLException {
		String sql = "INSERT INTO RoomTypes(Name, Capacity, BasePrice) VALUES (?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
		
		for (RoomType rt : roomTypes) {
			statement.setString(1, rt.getName());
			statement.setInt(2, rt.getCapacity());
			statement.setInt(3, rt.getBasePrice());
			statement.addBatch();
		}
		statement.executeBatch();
		try (ResultSet resultSet = statement.getGeneratedKeys()) {
			Iterator<RoomType> it = roomTypes.iterator();
			while (resultSet.next()) {
				it.next().setId(resultSet.getInt(1));
			}
		}
	}
	
	public RoomType selectRandomRoomType(int nHosts) throws SQLException {
		RoomType roomType = null;
		String sql = "SELECT rt.* FROM RoomTypes rt "
				+ "JOIN Rooms r ON r.RoomTypeId = rt.Id "
				+ "WHERE rt.Capacity>=? "
				+ "ORDER BY rt.Capacity, RAND() LIMIT 1";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setInt(1, nHosts);

		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				roomType = new RoomType(rs.getString("Name"), rs.getInt("Capacity"), rs.getInt("BasePrice"));
				roomType.setId(rs.getInt("Id"));
			}
		}
		return roomType;
	}

	public int selectRoomTypeIdByName(String name) throws SQLException {
		int roomTypeId=-1;
		String sql = "SELECT Id FROM RoomTypes WHERE Name=?";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setString(1, name);
		
		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				roomTypeId = rs.getInt(1);
			}
		}
		return roomTypeId;
	}
}
