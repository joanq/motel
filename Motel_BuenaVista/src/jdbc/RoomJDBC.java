package jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;

import models.Room;

public class RoomJDBC {

	public void insertRooms(List<Room> roomsList) throws SQLException {
		String sql = "INSERT INTO Rooms(RoomTypeId) VALUES (?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
		
		for (Room rt : roomsList) {
			statement.setInt(1, rt.getRoomType());
			statement.addBatch();
		}
		statement.executeBatch();
		Iterator<Room> it = roomsList.iterator();
		try (ResultSet rs = statement.getGeneratedKeys()) {
			while (rs.next()) {
				it.next().setRoomNumber(rs.getInt(1));
			}
		}
	}
	
	public int selectFreeRoomNumber(int roomTypeId) throws SQLException {
		int roomNumber=-1;
		String sql = "SELECT RoomNumber FROM Rooms WHERE RoomTypeId = ? AND Empty = TRUE ORDER BY RAND() LIMIT 1";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		
		st.setInt(1, roomTypeId);
		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				roomNumber = rs.getInt(1);
			}
		}
		return roomNumber;
	}
		
	public void updateRoomStatus(int roomId, Boolean status) throws SQLException {
		String sql = "UPDATE Rooms SET Empty = ? WHERE RoomNumber = ?";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setBoolean(1, status);
		st.setInt(2, roomId);
		st.executeUpdate();
	}

}
