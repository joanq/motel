package jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;

import models.Document;
import models.Host;

public class HostJDBC {	
	public void insertHost(Host host) throws SQLException {
		String sql = "INSERT INTO Hosts(DocType, DocNumber, FirstName, LastName, Nationality) VALUES (?,?,?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
					
		statement.setString(1, host.getDocument().doctype);
		statement.setString(2, host.getDocument().docnumber);
		statement.setString(3, host.getName());
		statement.setString(4, host.getLastname());
		statement.setString(5, host.getNationality());			
		statement.executeQuery();
		ResultSet rs = statement.getGeneratedKeys();
		if (rs.next()) {
			host.setId(rs.getInt(1));
		}
	}
	
	public boolean docnumberExists(String docnumber) throws SQLException {
		boolean exists=false;
		String sql = "SELECT CASE WHEN EXISTS (SELECT * FROM Hosts WHERE DocNumber=?) THEN 1 ELSE 0 END";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setString(1, docnumber);
		try (ResultSet rs = st.executeQuery();) {
			rs.next();
			if (rs.getInt(1)==1) {
				exists=true;
			}
		}
		return exists;
	}
	
	public Host getHostForCustomer(int customerId) throws SQLException {
		Host host = null;
		String sql = "select h.* "
				+ "from Customers c "
				+ "join Bookings b on b.CustomerId=c.Id "
				+ "join Stays s on b.Id=s.BookingId "
				+ "join StayHosts sh on s.Id=sh.StayId "
				+ "join Hosts h on h.Id=sh.HostId "
				+ "where c.Id=? "
				+ "and c.FirstName=h.FirstName "
				+ "and c.LastName=h.LastName "
				+ "and c.Nationality=h.Nationality";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setInt(1, customerId);
		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				host = new Host(
						rs.getString("FirstName"),
						rs.getString("LastName"),
						rs.getString("Nationality"),
						new Document(rs.getString("DocType"), rs.getString("DocNumber")));
				host.setId(rs.getInt("Id"));
			}
			
		}
		return host;
	}
	
	public List<Host> getHostsForStay(int stayId) throws SQLException {
		List<Host> hosts = new ArrayList<>();
		Host host;
		String sql = "SELECT h.* "
				+ "FROM Hosts h "
				+ "JOIN StayHosts sh ON sh.HostId=h.Id "
				+ "JOIN Stays s ON s.Id=sh.StayId "
				+ "WHERE s.Id=? ";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setInt(1, stayId);
		try (ResultSet rs = st.executeQuery();) {
			while (rs.next()) {
				host = new Host(
						rs.getString("FirstName"),
						rs.getString("LastName"),
						rs.getString("Nationality"),
						new Document(rs.getString("DocType"), rs.getString("DocNumber")));
				host.setId(rs.getInt("Id"));
				hosts.add(host);
			}
		}
		return hosts;
	}
	
	public Host getRandomHost() throws SQLException {
		Host host = null;
		String sql = "SELECT h.* FROM Hosts h ORDER BY RAND() LIMIT 1";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				host = new Host(
						rs.getString("FirstName"),
						rs.getString("LastName"),
						rs.getString("Nationality"),
						new Document(rs.getString("DocType"), rs.getString("DocNumber")));
				host.setId(rs.getInt("Id"));
			}
		}
		return host;
	}
}
