package jdbc;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;

import models.Season;

public class SeasonsJDBC {

	public void insertSeasons(List<Season> seasons) throws SQLException {
		String sql = "INSERT INTO Seasons(StartingDay,Name) VALUES (?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
		for (Season s : seasons) {
			statement.setDate(1, Date.valueOf(s.getStringDate()));
			statement.setString(2, s.getSeasonName());
			statement.addBatch();
		}
		statement.executeBatch();
		Iterator<Season> it = seasons.iterator();
		try (ResultSet rs = statement.getGeneratedKeys()) {
			while (rs.next()) {
				it.next().setId(rs.getInt(1));
			}
		}
		insertSeasonsCache(seasons);
	}
	
	private void insertSeasonsCache(List<Season> seasons) throws SQLException {
		String sql = "INSERT INTO SeasonsCache(Id,StartingDay,Name) VALUES (?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql);
		for (Season s : seasons) {
			statement.setInt(1, s.getId());
			statement.setDate(2, Date.valueOf(s.getStringDate()));
			statement.setString(3, s.getSeasonName());
			statement.addBatch();
		}
		statement.executeBatch();
	}
}
