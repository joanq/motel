package jdbc;

import java.sql.SQLException;

import com.violentbits.dbutils.dao.DataAccess;

public class CreateTablesJDBC {
	public static void createTables(String[] tables) throws SQLException {
		for(String table : tables) {
			DataAccess.getInstance().executeUpdate(table);
		}
	}
}
