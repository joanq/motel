package jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;

import models.Facilities;

public class FacilitiesJDBC {
	public void insertFacilities(List<Facilities> facilities) throws SQLException {
		String sql = "INSERT INTO Facilities(Name) VALUES (?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql);
			
		for(Facilities f: facilities) {
			statement.setString(1,f.getName());
			statement.addBatch();
		}
		statement.executeBatch();
	}

	public int selectFacilityByName(String name) throws SQLException {
		int facilityId=-1;
		String sql = "SELECT Id FROM Facilities WHERE Name=?";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setString(1, name);
		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				facilityId = rs.getInt(1);
			}
		}
		return facilityId;
	}
}
