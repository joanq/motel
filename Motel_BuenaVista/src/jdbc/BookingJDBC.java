package jdbc;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;

import models.Booking;

public class BookingJDBC {	
	public List<Booking> selectBookingsByCheckIn(LocalDate date) throws SQLException {
		List<Booking> bookingList = new ArrayList<Booking>();
		String sql = "SELECT * FROM PendingBookings WHERE CheckIn=?";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setDate(1, Date.valueOf(date));
		try (ResultSet rs = st.executeQuery();) {
			while (rs.next()) {
				Booking booking = new Booking(rs.getTimestamp("ReservationDateTime").toLocalDateTime(),
						rs.getInt("CustomerId"), rs.getInt("RoomTypeId"), rs.getDate("CheckIn").toLocalDate(),
						rs.getDate("CheckOut").toLocalDate(), rs.getDouble("Price"), rs.getString("State"),
						rs.getInt("NHosts"));
				booking.setId(rs.getInt("Id"));
				bookingList.add(booking);
			}
		}
		return bookingList;
	}
	
	public void deleteBooking(LocalDate date) throws SQLException {
		String sql = "DELETE FROM PendingBookings WHERE CheckOut=?";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql);

		statement.setDate(1, Date.valueOf(date));
		statement.executeUpdate();
	}

	public void insertBooking(Booking booking) throws SQLException {
		String sql = "INSERT INTO Bookings(ReservationDateTime, CustomerId,RoomTypeId,CheckIn,CheckOut,Price,State,NHosts) VALUES (?,?,?,?,?,?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql, Statement.RETURN_GENERATED_KEYS);

		statement.setTimestamp(1, Timestamp.valueOf(booking.getReservationDateTime()));
		statement.setInt(2, booking.getCustomerId());
		statement.setInt(3, booking.getRoomTypeId());
		statement.setDate(4, Date.valueOf(booking.getCheckIn()));
		statement.setDate(5, Date.valueOf(booking.getCheckOut()));
		statement.setDouble(6, booking.getPrice());
		statement.setString(7, booking.getState());
		statement.setInt(8, booking.getNhosts());
		statement.executeUpdate();
		
		try (ResultSet rs = statement.getGeneratedKeys()) {
			rs.next();
			booking.setId(rs.getInt(1));
			if (booking.getState().equals("Reserved")) {
				insertPendingBooking(booking);
			}
		}
	}
	
	private void insertPendingBooking(Booking booking) throws SQLException {
		String sql = "INSERT INTO PendingBookings(Id, ReservationDateTime, CustomerId, RoomTypeId, CheckIn, CheckOut, Price, State, NHosts) VALUES (?,?,?,?,?,?,?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql);

		statement.setInt(1, booking.getId());
		statement.setTimestamp(2, Timestamp.valueOf(booking.getReservationDateTime()));
		statement.setInt(3, booking.getCustomerId());
		statement.setInt(4, booking.getRoomTypeId());
		statement.setDate(5, Date.valueOf(booking.getCheckIn()));
		statement.setDate(6, Date.valueOf(booking.getCheckOut()));
		statement.setDouble(7, booking.getPrice());
		statement.setString(8, booking.getState());
		statement.setInt(9, booking.getNhosts());
		statement.executeUpdate();
	}
}
