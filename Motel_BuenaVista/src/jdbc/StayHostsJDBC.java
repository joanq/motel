package jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;

import models.StayHost;

public class StayHostsJDBC {
	
	public void insertStayHosts(List<StayHost> stayHosts) throws SQLException {
		String sql = "INSERT INTO StayHosts(StayId, HostId) VALUES (?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql);
		
		for (StayHost sh : stayHosts) {
			statement.setInt(1, sh.getStayId());
			statement.setInt(2, sh.getHostId());
			statement.addBatch();
		}
		statement.executeBatch();
	}
}
