package jdbc;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import com.violentbits.dbutils.dao.DataAccess;

import models.Booking;

public class BookingCalendarJDBC {
	public void insertBooking(LocalDate date, Booking booking, int roomNumber) throws SQLException {
		String sql = "INSERT INTO BookingCalendar(BookedDate, AssignedRoom, BookingId) VALUES (?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql, Statement.RETURN_GENERATED_KEYS);
		statement.setDate(1, Date.valueOf(date));
		statement.setInt(2, roomNumber);
		statement.setInt(3, booking.getId());
		statement.executeUpdate();
	}
	
	public Integer getFreeRoom(LocalDate startDate, LocalDate endDate, int roomTypeId) throws SQLException {
		Integer roomNumber = null;
		String sql = "SELECT r.RoomNumber "
				+ "FROM Rooms r "
				+ "WHERE r.RoomTypeId=? "
				+ "AND NOT EXISTS (SELECT * "
				+ "FROM BookingCalendar bc "
				+ "WHERE bc.AssignedRoom=r.RoomNumber AND BookedDate>=? AND BookedDate<?) "
				+ "ORDER BY r.RoomNumber LIMIT 1";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setInt(1, roomTypeId);
		st.setDate(2, Date.valueOf(startDate));
		st.setDate(3, Date.valueOf(endDate));
		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				roomNumber = rs.getInt("RoomNumber");
			}
		}
		return roomNumber;
	}
	
	public Integer getAssignedRoom(int bookingId) throws SQLException {
		Integer roomNumber=null;
		String sql = "SELECT AssignedRoom "
				+ "FROM BookingCalendar bc "
				+ "WHERE BookingId=?";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setInt(1, bookingId);
		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				roomNumber = rs.getInt("AssignedRoom");
			}
		}
		return roomNumber;
	}
}
