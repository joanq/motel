package jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.sql.Date;

import com.violentbits.dbutils.dao.DataAccess;

import models.Customer;

public class CustomerJDBC {	
	public void insertCustomer(Customer customer) throws SQLException {
		String sql = "INSERT INTO Customers(Email, FirstName, LastName, PhoneNumber, Nationality) VALUES (?,?,?,?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
		
		statement.setString(1, customer.getEmail());
		statement.setString(2, customer.getName());
		statement.setString(3, customer.getLastname());
		statement.setString(4, customer.getPhoneNumber());
		statement.setString(5, customer.getNationality());
		
		statement.executeUpdate();
		ResultSet rs = statement.getGeneratedKeys();
		if (rs.next()) {
			customer.setId(rs.getInt(1));
		}
	}
	
	public boolean emailExists(String email) throws SQLException {
		boolean exists = false;
		String sql = "SELECT CASE WHEN EXISTS (SELECT * FROM Customers WHERE Email=?) THEN 1 ELSE 0 END";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setString(1, email);
		try (ResultSet rs = st.executeQuery();) {
			rs.next();
			if (rs.getInt(1)==1) {
				exists = true;
			}
		}
		return exists;
	}
	
	public Customer selectCustomerbyId(int id) throws SQLException {
		Customer c = null;
		String sql = "SELECT * FROM Customers WHERE Id=?";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setInt(1, id);
		try (ResultSet rs = st.executeQuery();) {
			while (rs.next()) {
				c = new Customer(rs.getString("FirstName"),rs.getString("LastName"),rs.getString("PhoneNumber"),rs.getString("Email"),rs.getString("Nationality"));
				c.setId(rs.getInt("Id"));
			}
		}
		return c;
	}
	
	public Customer getRandom(LocalDate checkIn) throws SQLException {
		Customer c = null;
		String sql = "SELECT * FROM Customers c "
				+ "WHERE NOT EXISTS (SELECT * FROM Bookings b WHERE b.CustomerId=c.Id AND b.CheckOut>?) "
				+ "ORDER BY RAND() LIMIT 1";
		PreparedStatement st = DataAccess.getInstance().preparedStatement(sql);
		st.setDate(1, Date.valueOf(checkIn));
		try (ResultSet rs = st.executeQuery();) {
			if (rs.next()) {
				c = new Customer(rs.getString("FirstName"),rs.getString("LastName"),rs.getString("PhoneNumber"),rs.getString("Email"),rs.getString("Nationality"));
				c.setId(rs.getInt("Id"));
			}
		}
		return c;
	}
}
