package jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.violentbits.dbutils.dao.DataAccess;

import models.RoomTypeFacilities;

public class RoomTypeFacilitiesJDBC {

	public void insertRoomTypesFacilities(List<RoomTypeFacilities> roomTypesFacilities) throws SQLException {
		String sql = "INSERT INTO RoomTypeFacilities (RoomTypeId, FacilityId) VALUES (?,?)";
		PreparedStatement statement = DataAccess.getInstance().preparedStatement(sql);
		
		for (RoomTypeFacilities rtf : roomTypesFacilities) {
			statement.setInt(1, rtf.getRoomTypeId() );
			statement.setInt(2, rtf.getFacilityId());
			statement.addBatch();
		}
		statement.executeBatch();
	}
}
