package readers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Properties;

public class ConfigReader {
	private String connector, ip, port, user, password, url = "";
	private LocalDate startDate, endDate;
	private int nRooms;
	private String db = "hotel";
	
	public void readConf() throws IOException {
		File file = new File("moteldb.conf");
		
		Properties prop = new Properties();
		
		try (InputStream in = new FileInputStream(file);) {
			prop.load(in);
			
			connector = prop.getProperty("connector");
			ip = prop.getProperty("ip");
			port = prop.getProperty("port");
			user = prop.getProperty("user");
			password = prop.getProperty("password");
			url = connector + "://" + ip + ":" + port + "/" + db;
			startDate = LocalDate.parse(prop.getProperty("startDate", "2018-01-01"));
			endDate = LocalDate.parse(prop.getProperty("endDate", "2018-06-01"));
			nRooms = Integer.parseInt(prop.getProperty("nRooms", "50"));
		}
	}
	
	public String getURL() {
		return this.url;
	}
	public String getUser() {
		return this.user;
	}
	public String getPassword() {
		return this.password;
	}
	public String getDB() {
		return this.db;
	}
	public String getIP() {
		return this.ip;
	}

	public LocalDate getStartDate() {
		return startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public int getNRooms() {
		return nRooms;
	}
}
