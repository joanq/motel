package readers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.violentbits.dbutils.csv.CSVReader;
import com.violentbits.dbutils.storage.DataStorage;
import com.violentbits.dbutils.storage.UniformDataStorage;
import com.violentbits.dbutils.storage.WeightedDataStorage;

import models.Facilities;
import models.RoomType;

public class Reader {
	public static final String ROOT = "data/";

	public static List<String> emails() throws IOException {
		return CSVReader.readOne(ROOT+"emails.csv");
	}

	public static List<String> dot() throws IOException {
		List<String> dot = new ArrayList<>();
		List<String[]> csv = CSVReader.read(ROOT+"nationalities.csv");
		csv.forEach((String[] line)->dot.add(line[1]));
		return dot;
	}
	
	private static DataStorage<String> names(String nationality, String filename, int nFieldsWithFrequencies) throws IOException {
		List<String[]> lines = CSVReader.read(filename);
		DataStorage<String> dataStorage;
		List<String> namesList = new ArrayList<>();
		List<Double> frequencies = new ArrayList<>();

		if (lines.get(0).length==nFieldsWithFrequencies) {// has frequencies
			for (String[] line : lines) {
				namesList.add(line[0]);
				frequencies.add(Double.parseDouble(line[nFieldsWithFrequencies-1]));
			}
			dataStorage = new WeightedDataStorage<>(namesList, frequencies);
		} else { // doesn't have frequencies
			for (String[] line : lines) {
				namesList.add(line[0]);
			}
			dataStorage = new UniformDataStorage<>(namesList);
		}
		return dataStorage;
	}
	
	public static DataStorage<String> names(String nationality) throws IOException {
		return names(nationality, ROOT+nationality+"_names.csv", 3);
	}

	public static DataStorage<String> lastNames(String nationality) throws IOException {
		return names(nationality, ROOT+nationality+"_lastnames.csv", 2);
	}

	public static List<Facilities> facilities() throws IOException {
		List<String> csv = CSVReader.readOne(ROOT+"facilities.csv");
		List<Facilities> facilities = new ArrayList<>();
		csv.forEach((String line)->facilities.add(new Facilities(line)));
		return facilities;
	}

	public static List<String[]> seasons() throws IOException {
		return CSVReader.read(ROOT+"seasons.csv");
	}
	
	public static WeightedDataStorage<Integer> nHosts() throws IOException {
		List<String[]> csv = CSVReader.read(ROOT+"nHosts.csv");
		WeightedDataStorage<Integer> nHostsDataStorage = null;
		List<Integer> nHosts = new ArrayList<>();
		List<Double> freqs = new ArrayList<>();
		for (String[] line : csv) {
			int num = Integer.parseInt(line[0]);
			freqs.add(Double.parseDouble(line[1]));
			nHosts.add(num);
		}
		nHostsDataStorage = new WeightedDataStorage<>(nHosts, freqs);
		return nHostsDataStorage;
	}
	
	public static WeightedDataStorage<RoomType> roomTypes() throws IOException {
		List<String[]> csv = CSVReader.read(ROOT+"roomTypes.csv");
		WeightedDataStorage<RoomType> roomTypeDataStorage = null;

		List<RoomType> roomTypes = new ArrayList<>();
		List<Double> freqs = new ArrayList<>();
		for (String[] line : csv) {
			String name = line[0];
			int quantity = Integer.parseInt(line[1]);
			int priceBase = Integer.parseInt(line[2]);
			freqs.add(Double.parseDouble(line[3]));
			roomTypes.add(new RoomType(name, quantity, priceBase));
		}
		roomTypeDataStorage = new WeightedDataStorage<>(roomTypes, freqs);
		return roomTypeDataStorage;
	}

	public static List<String[]> roomTypesFacilities() throws IOException {
		return CSVReader.read(ROOT+"roomTypesFacilities.csv");
	}

	public static String[] createTables() throws IOException {
		String[] tables;
		try (Stream<String> stream = Files.lines(Paths.get(ROOT+"createTables.sql"))) {
			tables = stream.collect(Collectors.joining("")).split(";");
		}
		return tables;
	}
}
