package models;

import java.time.LocalDate;

public class Season {
	private int id;
	private LocalDate stringDate;
	private String seasonName = null;
	private double seasonPrice;

	public Season(String name, LocalDate date, double price) {
		setSeasonName(name);
		this.stringDate = date;
		setseasonPrice(price);
	}

	public void setSeasonName(String name) {
		this.seasonName = name;
	}

	public void setId(int id) {
		this.id = id;
		
	}
	public int getId() {
		return this.id;
	}
	public void setseasonPrice(double price) {
		this.seasonPrice = price;
	}

	public double getSeasonPrice() {
		return seasonPrice;
	}

	public void setSeasonPrice(double seasonPrice) {
		this.seasonPrice = seasonPrice;
	}

	public String getSeasonName() {
		return seasonName;
	}

	public LocalDate getStringDate() {
		return stringDate;
	}

	public void setStringDate(LocalDate stringDate) {
		this.stringDate = stringDate;
	}

	@Override
	public String toString() {
		return "Season [seasonName=" + seasonName + ", seasonPrice=" + seasonPrice + "]";
	}
}
