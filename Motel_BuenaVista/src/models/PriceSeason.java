package models;

public class PriceSeason {
	private double price;
	private Season season;
	private RoomType roomType;

	public PriceSeason(RoomType roomType, Season season) {
		super();
		this.season = season;
		this.roomType = roomType;
		calculatePrice();
	}
	
	private void calculatePrice() {
		price = roomType.getBasePrice() * season.getSeasonPrice();
	}
	
	public Season getSeason() {
		return season;
	}
	
	public RoomType getRoomType() {
		return roomType;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
