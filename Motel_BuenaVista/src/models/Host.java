package models;

public class Host {	
	String name;
	String lastname;
	String nationality;
	private Document document;
	int id;
	
	public Host() {

	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Host(String name, String lastname, String nationality, Document document) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.nationality = nationality;
		this.document = document;
	}

	//GETTERS AND SETTERS
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	
	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	public Document getDocument() {
		return document;
	}


	public void setDocument(Document document) {
		this.document = document;
	}

	@Override
	public String toString() {
		return "Host [name=" + name + ", lastname=" + lastname + ", nationality=" + nationality + ", docType=" + document.doctype
				+ ", docNumber=" + document.docnumber + "]";
	}
	

}
