package models;

public class Nationality {
	public final String name;
	public final String domain;
	public final int nLastnames;
	public final String doctype;
	public final String phonePrefix;
	
	public Nationality(String name, String domain, int nLastnames, String doctype, String phonePrefix) {
		this.name = name;
		this.domain = domain;
		this.nLastnames = nLastnames;
		this.doctype = doctype;
		this.phonePrefix = phonePrefix;
	}
}
