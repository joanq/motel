package models;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Stay {
	private int id;
	private LocalDate checkIn;
	private int roomNumber;
	private LocalDate checkOut;
	private double totalPrice;
	private String paymentType;
	private LocalDateTime paymentDateTime;
	private int bookingId;

	public Stay(LocalDate checkIn, int roomNumber, LocalDate checkOut, double totalPrice, String paymentType,
			LocalDateTime paymentDateTime, int bookingId) {
		super();
		this.checkIn = checkIn;
		this.roomNumber = roomNumber;
		this.checkOut = checkOut;
		this.totalPrice = totalPrice;
		this.paymentType = paymentType;
		this.paymentDateTime = paymentDateTime;
		this.bookingId = bookingId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(LocalDate checkIn) {
		this.checkIn = checkIn;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public LocalDate getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(LocalDate checkOut) {
		this.checkOut = checkOut;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public LocalDateTime getPaymentDateTime() {
		return paymentDateTime;
	}

	public void setPaymentDateTime(LocalDateTime paymentDateTime) {
		this.paymentDateTime = paymentDateTime;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	@Override
	public String toString() {
		return "Stay [id=" + id + ", checkIn=" + checkIn + ", roomNumber=" + roomNumber + ", checkOut=" + checkOut
				+ ", totalPrice=" + totalPrice + ", paymentType=" + paymentType 
				+ ", PaymentDateTime=" + paymentDateTime + ", bookingId=" + bookingId
				+ "]";
	}
}
