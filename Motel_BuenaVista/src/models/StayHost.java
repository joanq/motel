package models;

public class StayHost {
	private int stayId;
	private int hostId;

	public int getStayId() {
		return stayId;
	}

	public void setStayId(int stayId) {
		this.stayId = stayId;
	}

	public int getHostId() {
		return hostId;
	}

	public void setHostId(int hostId) {
		this.hostId = hostId;
	}

	public StayHost(int stayId, int hostId) {
		this.stayId = stayId;
		this.hostId = hostId;
	}

}
