package models;

public class Document {
	public final String doctype;
	public final String docnumber;
	
	public Document(String doctype, String docnumber) {
		this.doctype = doctype;
		this.docnumber = docnumber;
	}
}
