package models;

public class Customer {
	private int id;
	private String name;
	private String lastname;
	private String nationality;
	private String phoneNumber;
	private String dot;
	private String email;
	
	public Customer() {

	}
	
	public Customer(String name, String lastname, String phoneNumber, String email, String nationality) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.nationality = nationality;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	//GETTERS AND SETTERS
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDot() {
		return dot;
	}

	public void setDot(String dot) {
		this.dot = dot;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", lastname=" + lastname + ", nationality=" + nationality + ", phoneNumber="
				+ phoneNumber + ", dot=" + dot + ", email=" + email + "]";
	}
}
