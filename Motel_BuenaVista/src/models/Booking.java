package models;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Booking {
	private int id;
	private LocalDateTime reservationDateTime;
	private int customerId;
	private int roomTypeId;
	private LocalDate checkIn;
	private LocalDate checkOut;
	private double price;
	private String state;
	private int nhosts;
	

	public Booking(LocalDateTime reservationDateTime, int customerId, int roomTypeId, LocalDate checkIn, LocalDate checkOut,
			double price, String state, int nhosts) {
		super();
		this.reservationDateTime = reservationDateTime;
		this.customerId = customerId;
		this.roomTypeId = roomTypeId;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.price = price;
		this.state = state;
		this.nhosts = nhosts;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LocalDateTime getReservationDateTime() {
		return reservationDateTime;
	}
	public void setReservationDateTime(LocalDateTime reservationDateTime) {
		this.reservationDateTime = reservationDateTime;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getRoomTypeId() {
		return roomTypeId;
	}
	public void setRoomTypeId(int roomTypeId) {
		this.roomTypeId = roomTypeId;
	}
	public LocalDate getCheckIn() {
		return checkIn;
	}
	public void setCheckIn(LocalDate checkIn) {
		this.checkIn = checkIn;
	}
	public LocalDate getCheckOut() {
		return checkOut;
	}
	public void setCheckOut(LocalDate checkOut) {
		this.checkOut = checkOut;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getNhosts() {
		return nhosts;
	}
	public void setNhosts(int nhosts) {
		this.nhosts = nhosts;
	}
	
	@Override
	public String toString() {
		return "Booking [id=" + id + ", reservationDateTime=" + reservationDateTime + ", customerId=" + customerId
				+ ", roomTypeId=" + roomTypeId + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", price=" + price
				+ ", state=" + state + ", nhosts=" + nhosts + "]";
	}
	
}
