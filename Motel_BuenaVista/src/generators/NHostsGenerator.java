package generators;

import java.io.IOException;

import com.violentbits.dbutils.storage.WeightedDataStorage;

import readers.Reader;

public class NHostsGenerator {
	private WeightedDataStorage<Integer> nHostsDataStorage;
	
	public NHostsGenerator() throws IOException {
		nHostsDataStorage = Reader.nHosts();
	}

	public int getNHosts() {
		return nHostsDataStorage.get();
	}
}
