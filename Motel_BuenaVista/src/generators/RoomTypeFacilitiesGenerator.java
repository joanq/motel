package generators;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.FacilitiesJDBC;
import jdbc.RoomTypeJDBC;
import models.RoomTypeFacilities;
import readers.Reader;

public class RoomTypeFacilitiesGenerator {
	public List<RoomTypeFacilities> getList() throws SQLException, IOException {
		List<RoomTypeFacilities> returnList = new ArrayList<>();
		List<String[]> listOfFacilities = Reader.roomTypesFacilities();
			
		FacilitiesJDBC fjdbc = new FacilitiesJDBC();
		RoomTypeJDBC rtjdbc = new RoomTypeJDBC();

		for(String[] arrayS : listOfFacilities) {
			String nameOfRoomType = arrayS[0];
			String nameOfFacility = arrayS[1];
			int facilityId = fjdbc.selectFacilityByName(nameOfFacility);
			int roomTypeId  =  rtjdbc.selectRoomTypeIdByName(nameOfRoomType);
			returnList.add(new RoomTypeFacilities(roomTypeId,facilityId));							
		}
		return returnList;
	}
}
