package generators;

import static java.util.concurrent.ThreadLocalRandom.current;

public class FrenchDocGenerator implements NationalIdDocGenerator {
	/**
	 * Return a random French ID number.
	 * 
	 * @return
	 */
	@Override
	public String getNationalId() {
		String num = "";
		
		for (int i = 0; i < 12; i++) {
			num += String.valueOf(current().nextInt(10));
		}
		return num;
	}
}
