package generators;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.PriceSeason;
import models.RoomType;
import models.Season;

public class PriceSeasonsGenerator {
	public List<PriceSeason> createPriceSeasons(List<Season> seasons, List<RoomType> roomTypes) throws SQLException {
		List<PriceSeason> priceSeasons = new ArrayList<>();
		
		for(RoomType roomType :roomTypes) {
			for(Season season : seasons) {
				PriceSeason priceSeason = new PriceSeason(roomType, season);
				priceSeasons.add(priceSeason);
			}
		}
		return priceSeasons;
	}
}
