package generators;

import java.io.IOException;
import java.util.List;

import models.Facilities;
import readers.Reader;

public class FacilityGenerator {
	private List<Facilities> facilities;
	
	/**
	 * Return a List with all facilities. for insert to the DB, Hard-Coded
	 * 
	 * @return One list of Facilities
	 */
	public FacilityGenerator() throws IOException {
		facilities = Reader.facilities();
	}
	
	public List<Facilities> getFacilities() {
		return facilities;
	}



}
