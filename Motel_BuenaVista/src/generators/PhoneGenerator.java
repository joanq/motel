package generators;

import static java.util.concurrent.ThreadLocalRandom.current;


import models.Customer;

public class PhoneGenerator {
	/**
	 * Return a random phone based of the nationality
	 * @param Customer
	 * @return one String with the number + prefix
	 */
	public String getPhone(Customer customer) {
		String number = "";
		
		for(int i = 0 ; i < 9 ; i++) {
			number = number + String.valueOf(current().nextInt(10));
		}
		String prefix = Generators.nationalityGenerator.getByName(customer.getNationality()).phonePrefix;
		return prefix + number;
	}

}
