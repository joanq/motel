package generators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.violentbits.dbutils.csv.CSVReader;
import com.violentbits.dbutils.storage.WeightedDataStorage;

import models.Nationality;
import readers.Reader;

public class NationalityGenerator {
	private WeightedDataStorage<Nationality> nationalityStorage;
	
	/**
	 * At the constructor only add two Nationalities for the moment.
	 * Feel free to add anything.
	 * Its a List of Strings only, but remember at the HostFactory add a new equal
	 * See the GitLab for more info.
	 * 
	 * https://gitlab.com/jordi72/motel
	 */
	public NationalityGenerator() throws IOException {
		List<Nationality> nationalities = new ArrayList<>();
		List<Double> freqs = new ArrayList<>();
		List<String[]> lines = CSVReader.read(Reader.ROOT+"nationalities.csv");
		
		for (String[] line : lines) {
			nationalities.add(new Nationality(
					line[0],
					line[1],
					Integer.parseInt(line[2]),
					line[3],
					line[4]
			));
			freqs.add(Double.parseDouble(line[5]));
		}
		nationalityStorage = new WeightedDataStorage<>(nationalities, freqs);
	}

	/**
	 * Return a random nationality 
	 * @return one String with the nationality
	 */
	public Nationality getNewNationality() {
		return nationalityStorage.get();
	}
	
	public Nationality getByName(String nationalityName) {
		List<Nationality> nationalities = nationalityStorage.getAll();
		for (Nationality n : nationalities) {
			if (n.name.equals(nationalityName)) {
				return n;
			}
		}
		return null;
	}
	
	public List<Nationality> getAll() {
		return nationalityStorage.getAll();
	}
}
