package generators;

import java.io.IOException;
import java.util.List;

import com.violentbits.dbutils.storage.WeightedDataStorage;

import models.RoomType;
import readers.Reader;

public class RoomTypeGenerator {
	private WeightedDataStorage<RoomType> roomTypeDataStorage;
	
	public RoomTypeGenerator() throws IOException {
		roomTypeDataStorage = Reader.roomTypes();
	}

	public RoomType getRoomType() {
		return roomTypeDataStorage.get();
	}
	
	public List<RoomType> getAllRoomTypes() {
		return roomTypeDataStorage.getAll();
	}
}
