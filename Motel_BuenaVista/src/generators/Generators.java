package generators;

import java.io.IOException;
import java.time.LocalDate;

public class Generators {
	public static DocGenerator docGenerator;
	public static EmailGenerator emailGenerator;
	public static FacilityGenerator facilityGenerator;
	public static NameGenerator nameGenerator;
	public static NationalityGenerator nationalityGenerator;
	public static PhoneGenerator phoneGenerator;
	public static PriceSeasonsGenerator priceSeasonsGenerator;
	public static RoomTypeFacilitiesGenerator roomTypeFacilitiesGenerator;
	public static SeasonsGenerator seasonsGenerator;
	public static RoomTypeGenerator roomTypeGenerator;
	public static NHostsGenerator nHostsGenerator;
	
	private Generators() {
		;
	}
	
	public static void init(LocalDate startDate, LocalDate endDate) throws IOException {
		
		docGenerator = new DocGenerator();
		emailGenerator = new EmailGenerator();
		facilityGenerator = new FacilityGenerator();
		nationalityGenerator = new NationalityGenerator();
		nameGenerator = new NameGenerator();
		phoneGenerator = new PhoneGenerator();
		priceSeasonsGenerator = new PriceSeasonsGenerator();
		roomTypeFacilitiesGenerator = new RoomTypeFacilitiesGenerator();
		seasonsGenerator = new SeasonsGenerator(startDate, endDate);
		roomTypeGenerator = new RoomTypeGenerator();
		nHostsGenerator = new NHostsGenerator();
	}
}
