package generators;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.violentbits.dbutils.storage.DataStorage;

import models.Nationality;
import readers.Reader;

public class NameGenerator {
	private Map<String, DataStorage<String>> names = new HashMap<>();
	private Map<String, DataStorage<String>> lastnames = new HashMap<>();
	
	/**
	 * Call readers for getting the names.
	 * 
	 */
	public NameGenerator() throws IOException {
		for (Nationality nationality : Generators.nationalityGenerator.getAll()) {
			names.put(nationality.name, Reader.names(nationality.name));
			lastnames.put(nationality.name, Reader.lastNames(nationality.name));
		}
	}
	
	/**
	 * Return a random name of the nationality
	 * @param String with the name
	 * @return one String with the name
	 */
	public String getName(Nationality nationality) {
		DataStorage<String> allNames = names.get(nationality.name);
		return allNames.get();
	}
	
	/**
	 * Return a random lastname of the nationality
	 * @param String with the lastname
	 * @return one String with the lastname
	 */
	public String getLastName(Nationality nationality) {
		int nLastnames=nationality.nLastnames;
		String lastname="";
		DataStorage<String> allLastnames = lastnames.get(nationality.name);
		for (int nLastname=0; nLastname<nLastnames;nLastname++) {
			lastname+=allLastnames.get();
			lastname+=(nLastname==nLastnames-1?"":" ");
		}
		return lastname.toUpperCase();
	}
	
	/**
	 * Return a random domain for the email of the nationality
	 * @param String with the nationality
	 * @return one String with the domain for the email.
	 */
	public String getDomain(Nationality nationality) {
		return nationality.domain;
	}

}
