package generators;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.lang.reflect.InvocationTargetException;

import models.Document;
import models.Nationality;

public class DocGenerator {
	
	public Document getDocument(Nationality nationality) {
		String doctype = getDoctype(nationality);
		String docnumber = getDocumentNumber(nationality, doctype);
		return new Document(doctype, docnumber);
	}
	
	/**
	 * Return the type of doc. DNI for spanish or Passport to english
	 * 
	 * @param nationality
	 *            is a String where contains the nationality
	 * @return one String of the random doc. (For example, the DNI 32154231W)
	 */
	private String getDocumentNumber(Nationality nationality, String doctype) {
		if (doctype.equals("Passport")) {
			return getPassport();
		} else if (doctype.equals("National ID")) {
			try {
				@SuppressWarnings("unchecked")
				Class<NationalIdDocGenerator> c = (Class<NationalIdDocGenerator>) Class.forName("generators."+nationality.name+"DocGenerator");
				return c.getDeclaredConstructor().newInstance().getNationalId();
			} catch (NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException | 
					InvocationTargetException |	SecurityException | ClassNotFoundException e) {
				throw new UnsupportedOperationException(e);
			}
		}
		throw new UnsupportedOperationException("Invalid doctype");
	}

	/**
	 * Return the document type for the nationality
	 * 
	 * @param String
	 *            nationality Where contains the nationality
	 * @return one String with the doctype.
	 */
	private String getDoctype(Nationality nationality) {
		return nationality.doctype;
	}

	/**
	 * Return a random passport example.Random numbers with 8-9 length.
	 * 
	 * @return one String with the doc number for english people.
	 */
	private String getPassport() {
		String passport = "";

		for (int i = 0; i < current().nextInt(2) + 8; i++) {
			passport = passport + String.valueOf(current().nextInt(10));
		}
		return String.valueOf(passport);
	}

}
