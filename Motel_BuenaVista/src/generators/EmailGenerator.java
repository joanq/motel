package generators;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.io.IOException;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import jdbc.CustomerJDBC;
import models.Customer;
import readers.Reader;

public class EmailGenerator {
	private List<String> domains;
	private CustomerJDBC customerJDBC = new CustomerJDBC();
	private static String[] TLDs = {"com","net","edu"};
	
	public EmailGenerator() throws IOException {
		domains = Reader.emails();
	}
	
	/**
	 * Return a random email.
	 * 
	 * @param Customer
	 *            customer.
	 * @return one String with the random generated email...
	 */
	public String getEmail(Customer customer) throws SQLException {
		String suffix = "";
		String domain = domains.get(current().nextInt(domains.size()));
		String nameReplaced = normalizer(customer.getName());
		String lastnameReplaced = normalizer(customer.getLastname());
		String tld = current().nextInt(10)<=3?customer.getDot():TLDs[current().nextInt(TLDs.length)];
		
		String email = nameReplaced.replace(" ", "") + "_" + lastnameReplaced.replace(" ", "") + "@" + domain + "."
				+ tld;
		while (customerJDBC.emailExists(email+suffix)) {
			suffix = String.valueOf(ThreadLocalRandom.current().nextInt(1, 1000));
			System.out.println("WARNING: Duplicated email");
		}
		return email+suffix;
	}

	public String normalizer(String s) {
		return Normalizer.normalize(s.toLowerCase(), Normalizer.Form.NFD).
				replaceAll("[^\\p{ASCII}]", "").replace("'","");
	}

}
