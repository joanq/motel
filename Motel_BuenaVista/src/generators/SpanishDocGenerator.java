package generators;

import static java.util.concurrent.ThreadLocalRandom.current;

public class SpanishDocGenerator implements NationalIdDocGenerator {
	private static final String DNILetters = "TRWAGMYFPDXBNJZSQVHLCKE";
	
	/**
	 * Return a random DNI example.
	 * 
	 * @return one String with the doc number for Spanish people..
	 */
	@Override
	public String getNationalId() {
		String dniNum = "";
		char letter;

		for (int i = 0; i < 8; i++) {
			dniNum = dniNum + String.valueOf(current().nextInt(10));
		}
		// Will be used to calculate the letter.
		int residue = Integer.valueOf(dniNum) % 23;
		letter = DNILetters.charAt(residue);
		// returning the complete DNI (Identity National Document)
		return dniNum + letter;
	}
}
