package generators;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import models.Season;
import readers.Reader;

public class SeasonsGenerator {
	private List<Season> seasons;

	public SeasonsGenerator(LocalDate startDate, LocalDate endDate) throws IOException {
		List<String[]> repeatingSeasons = Reader.seasons();
		double priceIncrement=0.02;
		seasons = new ArrayList<>();
		int nYear=0;
		for (int year=startDate.getYear()-1; year<=endDate.getYear()+1; year++) {
			for (String[] repeatingSeason : repeatingSeasons) {
				int month = Integer.parseInt(repeatingSeason[1].split("-")[0]);
				int day = Integer.parseInt(repeatingSeason[1].split("-")[1]);
				double price = Double.parseDouble(repeatingSeason[2])+nYear*priceIncrement;
				seasons.add(new Season(repeatingSeason[0]+" "+year, LocalDate.of(year, month, day), price));
			}
			nYear++;
		}
	}

	public List<Season> getSeasons() {
		return seasons;
	}
}
