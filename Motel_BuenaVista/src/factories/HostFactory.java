package factories;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import generators.DocGenerator;
import generators.Generators;
import generators.NameGenerator;
import generators.NationalityGenerator;
import jdbc.HostJDBC;
import jdbc.StayJDBC;
import models.Customer;
import models.Document;
import models.Host;
import models.Nationality;

public class HostFactory {
	private static HostFactory instance;
	private HostJDBC hostJDBC = new HostJDBC();
	private StayJDBC stayJDBC = new StayJDBC();

	/**
	 * Call NationalityGenerator, where going to call the class for new Random
	 * Nationality.
	 */
	private NationalityGenerator nationalities = Generators.nationalityGenerator;
	/**
	 * Call NameGenerator. where create a new random Name and Last(s) names, need
	 * nationality.
	 */

	private NameGenerator names = Generators.nameGenerator;
	/**
	 * Call DocGenerator for new document, Needs nationality too!
	 */
	private DocGenerator docs = Generators.docGenerator;
	
	private HostFactory() {
		;
	}

	/**
	 * Return the instance , the unique object of this class.
	 * 
	 * @return
	 */
	public static HostFactory getInstance() {
		if (instance == null)
			instance = new HostFactory();
		return instance;
	}
	
	public List<Host> createHostsForCustomer(Customer customer, int nHosts) throws SQLException {
		boolean duplicatedCustomer = true;
		boolean repeatingCustomer = true;
		List<Host> hosts = new ArrayList<>();
		Host hostCustomer = hostJDBC.getHostForCustomer(customer.getId());
		if (hostCustomer==null) {
			repeatingCustomer = false;
			while (duplicatedCustomer) {
				hostCustomer = createHostCustomer(customer);
				if (!hostJDBC.docnumberExists(hostCustomer.getDocument().docnumber)) {
					hostJDBC.insertHost(hostCustomer);
					duplicatedCustomer = false;
				}
			}
		}
		hosts.add(hostCustomer);
		if (repeatingCustomer && current().nextDouble()<0.7) {
			// Comes with the same people
			Integer stayId = stayJDBC.getStayIdForCustomer(customer.getId());
			hosts.clear();
			hosts.addAll(hostJDBC.getHostsForStay(stayId));
		} else {
			hosts.addAll(createAdditionalHosts(customer, nHosts-1));
		}
		return hosts;
	}
	
	private List<Host> createAdditionalHosts(Customer c, int nHosts) throws SQLException {
		List<Host> hosts = new ArrayList<>();
		int extraHosts = nHosts;
		boolean duplicatedHost;
		
		for (int i = 0; i < extraHosts; i++) {
			Host h = null;
			if (current().nextDouble()<0.05) {
				// Repeating host
				h = hostJDBC.getRandomHost();
			}
			if (h == null) {
				// New host
				duplicatedHost = true;
				while (duplicatedHost) {
					h = createHost();
					if (!hostJDBC.docnumberExists(h.getDocument().docnumber)) {
						hostJDBC.insertHost(h);	
						hosts.add(h);
						duplicatedHost = false;
					} else {
						System.out.println("WARNING: Duplicated host");
					}
				}
			}
		}
		return hosts;
	}

	/**
	 * Create a random host.
	 * 
	 * @return The generated host.
	 */
	private Host createHost() {
		Host h = new Host();
		// Obtenim nacionalitat
		Nationality nationality = nationalities.getNewNationality();
		h.setNationality(nationality.name);
		h.setName(names.getName(nationality));
		h.setLastname(names.getLastName(nationality));
		Document doc = docs.getDocument(nationality);
		h.setDocument(doc);
		return h;
	}

	/**
	 * Converter from customer to host.
	 * 
	 * @return The generated host.
	 */
	private Host createHostCustomer(Customer c) {
		Host h = new Host();
		// Obtenim nacionalitat
		h.setNationality(c.getNationality());
		h.setName(c.getName());
		h.setLastname(c.getLastname());
		Document doc = docs.getDocument(nationalities.getByName(h.getNationality()));
		h.setDocument(doc);
		return h;
	}

}
