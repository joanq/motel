package factories;

import java.sql.SQLException;

import generators.EmailGenerator;
import generators.Generators;
import generators.NameGenerator;
import generators.NationalityGenerator;
import generators.PhoneGenerator;
import models.Customer;
import models.Nationality;

public class CustomerFactory {
	private static CustomerFactory instance;
	/**
	 * Call NationalityGenerator, where going to call the class for new Random
	 * Nationality.
	 */
	private NationalityGenerator nationalities = Generators.nationalityGenerator;
	/**
	 * Call NameGenerator. where create a new random Name and Last(s) names, need
	 * nationality.
	 */
	private NameGenerator names = Generators.nameGenerator;
	/**
	 * Call EmailGeneator for new email
	 */
	private EmailGenerator emails = Generators.emailGenerator;
	/**
	 * Call PhomeGenerator for new phone.
	 */
	private PhoneGenerator phones = Generators.phoneGenerator;
	
	private CustomerFactory() {
		;
	}

	/**
	 * Return the instance , the unique object of this class.
	 * 
	 * @return
	 */
	public static CustomerFactory getInstance() {
		if (instance == null)
			instance = new CustomerFactory();
		return instance;
	}

	/**
	 * Create a random customer
	 * 
	 * @return The generated customer.
	 */
	public Customer createCustomer() throws SQLException {
		Customer c = new Customer();
		Nationality nationality = nationalities.getNewNationality();
		c.setNationality(nationality.name);
		c.setName(names.getName(nationality));
		c.setLastname(names.getLastName(nationality));
		c.setDot(names.getDomain(nationality));
		c.setEmail(emails.getEmail(c));
		c.setPhoneNumber(phones.getPhone(c));
		return c;
	}

}
