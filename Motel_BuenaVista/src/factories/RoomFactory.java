package factories;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import generators.Generators;
import models.Room;

public class RoomFactory {
	public List<Room> createRooms(int nRooms) throws SQLException {
		List<Room> rooms = new ArrayList<>();
		for(int nRoom=0; nRoom<nRooms; nRoom++) {
			rooms.add(createRoom());
		}
		return rooms;
	}
	
	public Room createRoom() {
		Room room = new Room();
		room.setEmpty(true);
		room.setRoomType(Generators.roomTypeGenerator.getRoomType().getId());
		return room;
	}
}
