package factories;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jdbc.BookingCalendarJDBC;
import jdbc.BookingJDBC;
import jdbc.CustomerJDBC;
import jdbc.RoomJDBC;
import jdbc.StayHostsJDBC;
import jdbc.StayJDBC;
import models.Booking;
import models.Host;
import models.Stay;
import models.StayHost;

public class StayFactory {
	private static StayFactory instance;

	public static StayFactory getInstance() {
		if (instance == null)
			instance = new StayFactory();
		return instance;
	}
	
	private StayJDBC stayJDBC = new StayJDBC();
	private BookingJDBC bookingJDBC = new BookingJDBC();
	private StayHostsJDBC stayHostsJDBC = new StayHostsJDBC();
	private RoomJDBC roomJDBC = new RoomJDBC();
	private CustomerJDBC customerJDBC = new CustomerJDBC();
	private BookingCalendarJDBC bookingCalendarJDBC = new BookingCalendarJDBC();
	
	public void checkOutStays(LocalDate baseDate) throws SQLException {
		stayJDBC.checkOut(baseDate);
		bookingJDBC.deleteBooking(baseDate);
	}
	
	public void createStays(LocalDate baseDate, LocalDate endDate) throws SQLException {
		String paymentType;
		int roomNumber;
		LocalDateTime paymentDateTime;
		Stay stay;
		List<Host> hosts;
		List<StayHost> stayHosts;
		List<Booking> bookings = bookingJDBC.selectBookingsByCheckIn(baseDate);
		// Create Stay based on Booking
		for (Booking booking : bookings) {
			roomNumber = bookingCalendarJDBC.getAssignedRoom(booking.getId());
			if (roomNumber >= 0) {
				if (booking.getCheckOut().isBefore(endDate)) {
					paymentType = current().nextDouble(1) <= 0.5 ? "Card":"Cash";
					paymentDateTime = booking.getCheckOut().atTime(
							current().nextInt(5, 12),
							current().nextInt(60),
							current().nextInt(60));
					stay = new Stay(booking.getCheckIn(), roomNumber, booking.getCheckOut(),
							booking.getPrice(), paymentType,
							paymentDateTime, booking.getId());
				} else {
					stay = new Stay(booking.getCheckIn(), roomNumber, null,
							booking.getPrice(), null, null, booking.getId());	
				}
				stayJDBC.insertStay(stay);
				roomJDBC.updateRoomStatus(roomNumber, false);
				// Create hosts
				hosts = HostFactory.getInstance().createHostsForCustomer(
						customerJDBC.selectCustomerbyId(booking.getCustomerId()), booking.getNhosts());
				stayHosts = new ArrayList<>();
				for (Host host : hosts) {
					stayHosts.add(new StayHost(stay.getId(), host.getId()));
				}
				stayHostsJDBC.insertStayHosts(stayHosts);
			} else {
				System.err.println("WARNING: Overbooking "+baseDate+" "+booking.getId());
			}
		}
	}
}
