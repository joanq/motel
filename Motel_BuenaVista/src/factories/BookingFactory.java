package factories;

import static java.util.concurrent.ThreadLocalRandom.current;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import generators.Generators;
import jdbc.BookingCalendarJDBC;
import jdbc.BookingJDBC;
import jdbc.CustomerJDBC;
import jdbc.PriceSeasonsJDBC;
import jdbc.RoomTypeJDBC;
import models.Booking;
import models.Customer;
import models.RoomType;

public class BookingFactory {

	private static BookingFactory instance;

	public static BookingFactory getInstance() {
		if (instance == null)
			instance = new BookingFactory();
		return instance;
	}

	private RoomTypeJDBC roomTypeJDCB = new RoomTypeJDBC();
	private BookingJDBC bookingJDBC = new BookingJDBC();
	private PriceSeasonsJDBC priceSeasonsJDBC = new PriceSeasonsJDBC();
	private CustomerJDBC customerJDBC = new CustomerJDBC();
	private BookingCalendarJDBC bookingCalendarJDBC = new BookingCalendarJDBC();
	
	private static final String RESERVED = "Reserved";
	private static final String CANCELLED = "Cancelled";
	
	public long timeCustomers, timeRoomTypes, timePrices, timeInserts;
	private long tmpTime;
	
	public void createBooking(LocalDate baseDate) throws SQLException {
		LocalDateTime reservationDateTime = baseDate.atTime(
				LocalTime.of(current().nextInt(23), current().nextInt(60), current().nextInt(60)));
		// TODO: better random bookingDate
		LocalDate checkInDate = baseDate.plusDays(current().nextInt(100));
		LocalDate checkOutDate = checkInDate.plusDays(current().nextInt(1, 9));
		tmpTime=System.currentTimeMillis();
		Customer customer = null;
		boolean repeatingCustomer=false;
		if (current().nextDouble(1)<0.05) {
			// Repeating customer
			customer = customerJDBC.getRandom(checkInDate);
			if (customer!=null) {
				repeatingCustomer = true;
			}
		}
		if (customer == null) {
			customer = CustomerFactory.getInstance().createCustomer();
		}
		timeCustomers+=System.currentTimeMillis()-tmpTime;
		if (current().nextDouble(1)>=0.01) {
			int nHosts = Generators.nHostsGenerator.getNHosts();
			// Get type of room
			tmpTime=System.currentTimeMillis();
			RoomType roomType = roomTypeJDCB.selectRandomRoomType(nHosts);
			timeRoomTypes+=System.currentTimeMillis()-tmpTime;
			if (roomType!=null) {
				// Get free room
				Integer roomNumber = bookingCalendarJDBC.getFreeRoom(checkInDate, checkOutDate, roomType.getId());
				if (roomNumber != null) {
					// Get price
					tmpTime=System.currentTimeMillis();
					double price = 0;
					for (LocalDate date=LocalDate.from(checkInDate); date.isBefore(checkOutDate); date=date.plusDays(1)) {
						price+=priceSeasonsJDBC.getBookingPrice(roomType.getId(), date);
					}
					timePrices+=System.currentTimeMillis()-tmpTime;
					tmpTime=System.currentTimeMillis();
					if (!repeatingCustomer) {
						customerJDBC.insertCustomer(customer);
					}
					// Cancellations
					String state = current().nextDouble(1) > 0.1 ? RESERVED : CANCELLED;
					// Create booking
					Booking booking = new Booking(reservationDateTime, customer.getId(), roomType.getId(), checkInDate,
							checkOutDate, price, state, nHosts);
					bookingJDBC.insertBooking(booking);
					if (state.equals(RESERVED)) {
						for (LocalDate date=LocalDate.from(checkInDate); date.isBefore(checkOutDate); date=date.plusDays(1)) {
							bookingCalendarJDBC.insertBooking(date, booking, roomNumber);
						}
					}
					timeInserts+=System.currentTimeMillis()-tmpTime;
				}
			}
		} else { // Customer registers, but doesn't make a booking
			if (!repeatingCustomer) {
				customerJDBC.insertCustomer(customer);
			}
		}
	}
}
