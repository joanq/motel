= Hostal - relacional
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

*Facilities*([underline]#Name#)

*RoomTypes*([underline]#Name#, Capacity, Shared)

*RoomTypeFacilities*([underline]#Facility, RoomType#)
  on _Facility_ referencia _Facilities_
  i on _RoomType_ referencia _RoomTypes_.

*Seasons*([underline]#StartingDay#, Name)

*PriceSeasons*([underline]#StartingDay, RoomType#, Price)
  on _StartingDay_ referencia _Seasons_
  i on _RoomType_ referencia _RoomTypes_.

*Rooms*([underline]#RoomNumber#, RoomType, State)
  on _RoomType_ referencia _RoomTypes_.

*Customers*([underline]#Email#, FirstName, LastName, PhoneNumber)

*Bookings*([underline]#ReservationDateTime, CustomerEmail#, RoomType, CheckIn, CheckOut, Price, State, NHosts)
  on _CustomerEmail_ referencia _Customers_,
  i on _RoomType_ referencia _RoomTypes_.

*Stays*([underline]#CheckIn, RoomNumber#, CheckOut, TotalPrice, PaymentType, PaymentDateTime, ReservationDateTime, CustomerEmail)
  on _RoomNumber_ referencia _Rooms_,
  i on _ReservationDateTime i _CustomerEmail_ referencien _Bookings_.

*Hosts*([underline]#DocType, DocNumber#, FirstName, LastName, Nationality)

*StayHosts*([underline]#CheckIn, RoomNumber, DocType, DocNumber#)
  on _CheckIn_ i _RoomNumber_ referencien _Stays_,
  i on _DocType_ i _DocNumber_ referencien _Hosts_.
